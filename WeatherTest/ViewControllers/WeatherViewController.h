//
//  WeatherViewController.h
//  WeatherTest
//
//  Created by Антон Тютин on 23.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForecastsCollectionViewDataSource.h"
#import "WeatherDataManager.h"

@interface WeatherViewController : UIViewController <WeatherDataManagerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@property (weak, nonatomic) IBOutlet UILabel *townLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *heatLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *cloudinessLabel;
@property (weak, nonatomic) IBOutlet UILabel *precipitationLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UILabel *wetLabel;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

@property (nonatomic) NSUInteger currentForecast;
@property (strong, nonatomic) ForecastsCollectionViewDataSource *collectionViewDataSource;

- (IBAction)updateAction:(id)sender;
- (void)setInfoForForecastAtIndex:(NSUInteger)index;

@end
