//
//  WeatherViewController.h
//  WeatherTest
//
//  Created by Антон Тютин on 21.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "WeatherViewController.h"

@interface iPhoneWeatherViewController : WeatherViewController <UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end
