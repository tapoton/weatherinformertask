//
//  iPadWeatherViewController.m
//  WeatherTest
//
//  Created by Антон Тютин on 22.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "iPadWeatherViewController.h"
#import "ForecastsCollectionViewDataSource.h"

@interface iPadWeatherViewController ()

@property (nonatomic) CGSize viewSize;
@end

@implementation iPadWeatherViewController

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending) {
         [self.collectionView performBatchUpdates:nil completion:nil];
    }
}

//- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
//    if ([currSysVer compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending) {
//        [self.collectionView performBatchUpdates:nil completion:nil];
//    }
//}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    self.viewSize = size;
    [self.collectionView performBatchUpdates:nil completion:nil];
}

#pragma mark - Collection view delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger numberOfCells = self.collectionViewDataSource.townInfo.forecasts.count;
    CGSize size = CGSizeEqualToSize(self.viewSize, CGSizeZero) ? self.collectionView.bounds.size : self.viewSize;
    size.width = self.collectionView.bounds.size.width;
    return CGSizeMake(size.width, (size.height - 20) / numberOfCells - 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self setInfoForForecastAtIndex:indexPath.row];
}

@end
