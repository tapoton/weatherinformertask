//
//  WeatherViewController.m
//  WeatherTest
//
//  Created by Антон Тютин on 23.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "WeatherViewController.h"

@interface WeatherViewController ()

@end

@implementation WeatherViewController

- (ForecastsCollectionViewDataSource *)collectionViewDataSource
{
    if (!_collectionViewDataSource) {
        _collectionViewDataSource = [[ForecastsCollectionViewDataSource alloc] init];
    }
    return _collectionViewDataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource = self.collectionViewDataSource;
    for (UILabel *label in self.labels) {
        label.text = @"";
    }
    [self updateInfo];
}

- (void)updateInfo
{
    WeatherDataManager *manager = [WeatherDataManager sharedManager];
    [manager load];
    
    // we use cache and try to refresh data
    if (manager.currentInfo) {
        TownInfo *townInfo = [manager.currentInfo[@"towns"] firstObject];
        self.collectionViewDataSource.townInfo = townInfo;
        self.townLabel.text = townInfo.name;
    }
    [manager setDelegate:self];
    [manager updateInfo];
}

- (void)updateAction:(id)sender
{
    [self updateInfo];
}

- (void)setInfoForForecastAtIndex:(NSUInteger)index
{
    if (self.collectionViewDataSource.townInfo.forecasts.count <= index) {
        return;
    }
    TownInfo *town = self.collectionViewDataSource.townInfo;
    self.townLabel.text = town.name;
    ForecastInfo *forecast = town.forecasts[index];
    NSString *dateText = [NSDateFormatter localizedStringFromDate:forecast.date
                                                        dateStyle:NSDateFormatterFullStyle
                                                        timeStyle:NSDateFormatterNoStyle];
    dateText = [[[dateText substringToIndex:1] capitalizedString] stringByAppendingString:[dateText substringFromIndex:1]];
    self.dateLabel.text = dateText;
    self.timeLabel.text = [NSDateFormatter localizedStringFromDate:forecast.date
                                                         dateStyle:NSDateFormatterNoStyle
                                                         timeStyle:NSDateFormatterLongStyle];
    self.temperatureLabel.text = [forecast temperatureDescription];
    self.heatLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Feels like: %@", nil), [forecast heatDescription]];
    self.cloudinessLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Cloudiness: %@", nil), [forecast cloudinessDescription]];
    self.pressureLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Pressure: %@", nil), [forecast pressureDescription]];
    self.precipitationLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Precipitation: %@", nil), [forecast precipitationDescription]];
    self.windLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Wind: %@", nil), [forecast windDescription]];
    self.wetLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Humidity: %@", nil), [forecast relwetDescription]];
}

#pragma mark - Weather data manager delegate methods

- (void)dataManager:(WeatherDataManager *)dataManager didUpdateInfo:(NSDictionary *)info
{
    if (info) {
        self.errorLabel.hidden = YES;
        TownInfo *townInfo = [info[@"towns"] firstObject];
        self.collectionViewDataSource.townInfo = townInfo;
        [self.collectionView reloadData];
        [self setInfoForForecastAtIndex:self.currentForecast];

    } else {
        self.errorLabel.hidden = NO;
    }
}

@end
