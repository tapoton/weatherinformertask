//
//  iPadWeatherViewController.h
//  WeatherTest
//
//  Created by Антон Тютин on 22.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "WeatherViewController.h"

@interface iPadWeatherViewController : WeatherViewController <UICollectionViewDelegateFlowLayout>

@end
