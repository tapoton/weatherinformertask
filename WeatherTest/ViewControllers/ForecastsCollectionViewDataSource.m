//
//  ForecastsCollectionViewDataSource.m
//  WeatherTest
//
//  Created by Антон Тютин on 22.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "ForecastsCollectionViewDataSource.h"
#import "ForecastCollectionViewCell.h"
#import "TownInfo.h"
#import "ForecastInfo.h"

@implementation ForecastsCollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.townInfo.forecasts count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TimeOfDayCell";
    ForecastCollectionViewCell *cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                                     forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(ForecastCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    ForecastInfo *forecast = self.townInfo.forecasts[indexPath.row];
    cell.temperatureLabel.text = [forecast temperatureDescription];
    NSString *timeOfDay;
    UIImage *image;
    switch (forecast.timeOfDay) {
        case Night:
            timeOfDay = NSLocalizedString(@"Night", nil);
            image = [UIImage imageNamed:@"night"];
            break;
        case Morning:
            timeOfDay = NSLocalizedString(@"Morning", nil);
            image = [UIImage imageNamed:@"morning"];
            break;
        case Day:
            timeOfDay = NSLocalizedString(@"Day", nil);
            image = [UIImage imageNamed:@"day"];
            break;
        case Evening:
            timeOfDay = NSLocalizedString(@"Evening", nil);
            image = [UIImage imageNamed:@"evening"];
            break;
        default:
            break;
    }
    cell.timeOfDayLabel.text = timeOfDay;
    cell.imageView.image = image;
}
@end
