//
//  ForecastsCollectionViewDataSource.h
//  WeatherTest
//
//  Created by Антон Тютин on 22.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TownInfo;
@interface ForecastsCollectionViewDataSource : NSObject <UICollectionViewDataSource>

@property (strong, nonatomic) TownInfo *townInfo;

@end
