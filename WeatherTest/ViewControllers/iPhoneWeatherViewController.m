//
//  WeatherViewController.m
//  WeatherTest
//
//  Created by Антон Тютин on 21.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "iPhoneWeatherViewController.h"
#import "ForecastsCollectionViewDataSource.h"

@implementation iPhoneWeatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageControl.hidden = YES;
    self.pageControl.currentPage = 0;
}

- (IBAction)pageControlValurChaned:(UIPageControl *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.currentPage inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath
                                atScrollPosition:UICollectionViewScrollPositionNone
                                        animated:YES];
    [self setInfoForForecastAtIndex:sender.currentPage];
}

#pragma mark - Weather data manager delegate methods

- (void)dataManager:(WeatherDataManager *)dataManager didUpdateInfo:(NSDictionary *)info
{
    [super dataManager:dataManager didUpdateInfo:info];
    if (info) {
        TownInfo *townInfo = [info[@"towns"] firstObject];
        self.pageControl.hidden = NO;
        self.pageControl.numberOfPages = townInfo.forecasts.count;
    }
}

#pragma mark - Collection view delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.bounds.size.width, collectionView.frame.size.height);
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.collectionView) {
        CGPoint center = CGPointMake(CGRectGetMidX(self.collectionView.bounds),
                                     CGRectGetMidY(self.collectionView.bounds));
        NSIndexPath *currentIndexPath = [self.collectionView indexPathForItemAtPoint:center];
        self.pageControl.currentPage = currentIndexPath.item;
        [self setInfoForForecastAtIndex:currentIndexPath.item];
    }
}
@end
