//
//  TownInfo.m
//  WeatherTest
//
//  Created by Антон Тютин on 21.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "TownInfo.h"
#import "ForecastInfo.h"

@implementation TownInfo

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.index = [dict[@"index"] integerValue];
        self.latitude = [dict[@"latitude"] floatValue];
        self.longitude = [dict[@"longitude"] floatValue];
        self.forecasts = [NSMutableArray array];
        self.name = [dict[@"sname"] stringByReplacingPercentEscapesUsingEncoding:NSWindowsCP1251StringEncoding];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.index = [aDecoder decodeIntegerForKey:@"index"];
        self.latitude = [aDecoder decodeFloatForKey:@"latitude"];
        self.longitude = [aDecoder decodeFloatForKey:@"longitude"];
        self.forecasts = [aDecoder decodeObjectForKey:@"forecasts"];
        for (ForecastInfo *forecast in self.forecasts) {
            forecast.town = self;
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.forecasts forKey:@"forecasts"];
    [aCoder encodeInteger:self.index forKey:@"index"];
    [aCoder encodeFloat:self.latitude forKey:@"latitude"];
    [aCoder encodeFloat:self.longitude forKey:@"longtude"];
}

@end
