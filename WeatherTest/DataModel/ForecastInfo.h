//
//  WeatherInfo.h
//  WeatherTest
//
//  Created by Антон Тютин on 20.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TimeOfDay) {
    Night = 0,
    Morning,
    Day,
    Evening
};

typedef NS_ENUM(NSUInteger, Weekday) {
    Sunday = 1,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
};

extern NSString *const kMaxKey;
extern NSString *const kMinKey;

@class TownInfo;

@interface ForecastInfo : NSObject <NSCoding>

@property (strong, nonatomic) NSDate *date;
@property (nonatomic) TimeOfDay timeOfDay;
@property (nonatomic) Weekday weekday;
@property (strong, nonatomic) NSDictionary *phenomena;
@property (strong, nonatomic) NSDictionary *wind;
@property (strong, nonatomic) NSDictionary *pressure;
@property (strong, nonatomic) NSDictionary *temperature;
@property (strong, nonatomic) NSDictionary *relativeWet;
@property (strong, nonatomic) NSDictionary *heat;

@property (weak, nonatomic) TownInfo *town;

- (instancetype)initWithDict:(NSDictionary *)dict;

- (NSString *)temperatureDescription;
- (NSString *)heatDescription;
- (NSString *)cloudinessDescription;
- (NSString *)pressureDescription;
- (NSString *)precipitationDescription;
- (NSString *)relwetDescription;
- (NSString *)windDescription;
@end
