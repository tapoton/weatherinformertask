//
//  WeatherDataManger.m
//  WeatherTest
//
//  Created by Антон Тютин on 21.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "WeatherDataManager.h"
#import "WeatherXMLParser.h"
#import <UIKit/UIKit.h>


static NSString *const kSavedInfoKey = @"SavedWeatherInfo";

@implementation WeatherDataManager

+ (instancetype)sharedManager
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (NSArray *)towns
{
    return self.currentInfo[@"towns"];
}

- (NSArray *)forecasts
{
    NSMutableArray *forecasts = [NSMutableArray array];
    for (TownInfo *town in self.towns) {
        [forecasts addObjectsFromArray:town.forecasts];
    }
    return forecasts;
}

- (void)save
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.currentInfo];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kSavedInfoKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)load
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedInfoKey];
    self.currentInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

- (void)updateInfo
{
    NSURL *url = [NSURL URLWithString:@"http://informer.gismeteo.ru/xml/34122_1.xml"];
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        app.networkActivityIndicatorVisible = NO;
        if (data) {
            WeatherXMLParser *parser = [[WeatherXMLParser alloc] init];
            [parser parseData:data withCompletion:^(NSDictionary *result, NSError *error) {
                if (result) {
                    [self processResultDictionary:result];
                    if ([self.delegate respondsToSelector:@selector(dataManager:didUpdateInfo:)]) {
                        [self.delegate dataManager:self didUpdateInfo:self.currentInfo];
                    }
                } else {
                    if (error) {
                        NSLog(@"%@", error);
                    }
                }
            }];
        }
    }] resume];
}

- (void)processResultDictionary:(NSDictionary *)dict
{
    NSMutableArray *towns = [NSMutableArray array];
    if (dict[@"TOWN"]) {
        for (NSDictionary *townDict in dict[@"TOWN"]) {
            TownInfo *townInfo = [[TownInfo alloc] initWithDict:townDict];
            if (townDict[@"FORECAST"]) {
                for (NSDictionary *forecastDict in townDict[@"FORECAST"]) {
                    ForecastInfo *forecastInfo = [[ForecastInfo alloc] initWithDict:forecastDict];
                    forecastInfo.town = townInfo;
                    [townInfo.forecasts addObject:forecastInfo];
                }
                townInfo.forecasts = [[townInfo.forecasts sortedArrayUsingComparator:^NSComparisonResult(ForecastInfo *obj1, ForecastInfo *obj2) {
                    NSComparisonResult comparisonResult = [obj1.date compare:obj2.date];
                    return comparisonResult;
                }] mutableCopy];
            }
            [towns addObject:townInfo];
        }
    }
    self.currentInfo = @{@"towns" : towns};
    [self save];
}


@end
