//
//  WeatherXMLParser.h
//  WeatherTest
//
//  Created by Антон Тютин on 20.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ParserCompletion)(NSDictionary *result, NSError *error);
@interface WeatherXMLParser : NSObject

- (void)parseData:(NSData *)data withCompletion:(ParserCompletion)completion;

@end
