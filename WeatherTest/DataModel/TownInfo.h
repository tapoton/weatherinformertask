//
//  TownInfo.h
//  WeatherTest
//
//  Created by Антон Тютин on 21.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TownInfo : NSObject <NSCoding>

@property (strong, nonatomic) NSString *name;
@property (nonatomic) NSInteger index;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) NSMutableArray *forecasts;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end
