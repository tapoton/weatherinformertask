//
//  WeatherInfo.m
//  WeatherTest
//
//  Created by Антон Тютин on 20.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "ForecastInfo.h"

NSString *const kMaxKey = @"max";
NSString *const kMinKey = @"min";

@interface ForecastInfo()

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@end

@implementation ForecastInfo

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.date = dict[@"date"];
        self.timeOfDay = [dict[@"tod"] integerValue];
        self.weekday = [dict[@"weekday"] integerValue];
        self.phenomena = dict[@"PHENOMENA"];
        self.pressure = dict[@"PRESSURE"];
        self.temperature = dict[@"TEMPERATURE"];
        self.wind = dict[@"WIND"];
        self.relativeWet = dict[@"RELWET"];
        self.heat = dict[@"HEAT"];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.date = [aDecoder decodeObjectForKey:@"date"];
        self.timeOfDay = [aDecoder decodeIntegerForKey:@"timeOfDay"];
        self.weekday = [aDecoder decodeIntegerForKey:@"weekday"];
        self.phenomena = [aDecoder decodeObjectForKey:@"phenomena"];
        self.pressure = [aDecoder decodeObjectForKey:@"pressure"];
        self.temperature = [aDecoder decodeObjectForKey:@"temperature"];
        self.wind = [aDecoder decodeObjectForKey:@"wind"];
        self.relativeWet = [aDecoder decodeObjectForKey:@"relwet"];
        self.heat = [aDecoder decodeObjectForKey:@"heat"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.date forKey:@"date"];
    [aCoder encodeInteger:self.timeOfDay forKey:@"timeOfDay"];
    [aCoder encodeInteger:self.weekday forKey:@"weekday"];
    [aCoder encodeObject:self.phenomena forKey:@"phenomena"];
    [aCoder encodeObject:self.pressure forKey:@"pressure"];
    [aCoder encodeObject:self.temperature forKey:@"temperature"];
    [aCoder encodeObject:self.wind forKey:@"wind"];
    [aCoder encodeObject:self.relativeWet forKey:@"relwet"];
    [aCoder encodeObject:self.heat forKey:@"heat"];
}

- (NSString *)temperatureDescription
{
    return [NSString stringWithFormat:@"%@..%@℃",
            self.temperature[kMinKey], self.temperature[kMaxKey]];
}

- (NSString *)heatDescription
{
    return [NSString stringWithFormat:@"%@..%@℃",
            self.heat[kMinKey], self.heat[kMaxKey]];
}

- (NSString *)cloudinessDescription
{
    // облачность по градациям:  0 - ясно, 1- малооблачно, 2 - облачно, 3 - пасмурно
    NSInteger cloudiness = [self.phenomena[@"cloudiness"] integerValue];
    switch (cloudiness) {
        case 0:
            return NSLocalizedString(@"Clear", nil);
        case 1:
            return NSLocalizedString(@"Partly cloudy", nil);
        case 2:
            return NSLocalizedString(@"Cloudy", nil);
        case 3:
            return NSLocalizedString(@"Mostly cloudy", nil);
        default:
            break;
    }
    return @"";
}

- (NSString *)pressureDescription
{
    return [NSString stringWithFormat:NSLocalizedString(@"%@-%@ mm Hg", nil), self.pressure[kMinKey], self.pressure[kMaxKey]];
}

- (NSString *)precipitationDescription
{
    // тип осадков: 4 - дождь, 5 - ливень, 6,7 – снег, 8 - гроза, 9 - нет данных, 10 - без осадков
    NSInteger precipation = [self.phenomena[@"precipitation"] integerValue];
    switch (precipation) {
        case 4:
            return NSLocalizedString(@"Rain", nil);
        case 5:
            return NSLocalizedString(@"Shower", nil);
        case 6:
        case 7:
            return NSLocalizedString(@"Snow", nil);
        case 8:
            return NSLocalizedString(@"Storm", nil);
        case 9:
            return NSLocalizedString(@"No data", nil);
        case 10:
            return NSLocalizedString(@"Without precipitation", nil);
        default:
            break;
    }
    return @"";
}

- (NSString *)relwetDescription
{
    return [NSString stringWithFormat:@"%@-%@%%", self.relativeWet[kMinKey], self.relativeWet[kMaxKey]];
}

- (NSString *)windDescription
{
    NSString *speedDescr = [NSString stringWithFormat:NSLocalizedString(@"%@-%@ mps", nil), self.wind[kMinKey], self.wind[kMaxKey]];
    NSInteger direction = [self.wind[@"direction"] integerValue];
    NSString *directionDescr = @"";
    switch (direction) {
        case 0:
            directionDescr = NSLocalizedString(@"north", nil);
            break;
        case 1:
            directionDescr = NSLocalizedString(@"northeast", nil);
            break;
        case 2:
            directionDescr = NSLocalizedString(@"east", nil);
            break;
        case 3:
            directionDescr = NSLocalizedString(@"southeast", nil);
            break;
        case 4:
            directionDescr = NSLocalizedString(@"south", nil);
            break;
        case 5:
            directionDescr = NSLocalizedString(@"southwest", nil);
            break;
        case 6:
            directionDescr = NSLocalizedString(@"west", nil);
            break;
        case 7:
            directionDescr = NSLocalizedString(@"northwest", nil);
            break;
        default:
            break;
    }
    return [NSString stringWithFormat:@"%@, %@", directionDescr, speedDescr];
}

@end
