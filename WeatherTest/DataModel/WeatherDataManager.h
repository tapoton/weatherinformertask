//
//  WeatherDataManger.h
//  WeatherTest
//
//  Created by Антон Тютин on 21.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TownInfo.h"
#import "ForecastInfo.h"


@protocol WeatherDataManagerDelegate;

@interface WeatherDataManager : NSObject
@property (weak, nonatomic) id<WeatherDataManagerDelegate> delegate;

@property (strong, nonatomic) NSDictionary *currentInfo;
@property (readonly, nonatomic) NSArray *towns;
@property (readonly, nonatomic) NSArray *forecasts;

+ (instancetype)sharedManager;

- (void)updateInfo;
- (void)save;
- (void)load;

@end

@protocol WeatherDataManagerDelegate <NSObject>
@optional
- (void)dataManager:(WeatherDataManager *)dataManager didUpdateInfo:(NSDictionary *)info;

@end