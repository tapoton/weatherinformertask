//
//  WeatherXMLParser.m
//  WeatherTest
//
//  Created by Антон Тютин on 20.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import "WeatherXMLParser.h"

@interface WeatherXMLParser () <NSXMLParserDelegate>

@property (strong, nonatomic) NSXMLParser *parser;
@property (copy, nonatomic) ParserCompletion parseCompletion;

@property (strong, nonatomic) NSMutableDictionary *currentTown;
@property (strong, nonatomic) NSMutableDictionary *currentForecast;

@property (strong, nonatomic) NSMutableDictionary *resultDict;

// we store date formatter in property because
// it takes a lot of time to create new one every time we get new forecast
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@end

@implementation WeatherXMLParser

- (NSDateFormatter *)dateFormatter
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"YYYY-MM-dd HH"];
    }
    return _dateFormatter;
}

- (void)parseData:(NSData *)data withCompletion:(ParserCompletion)completion
{
    self.parseCompletion = completion;
    self.resultDict = [NSMutableDictionary dictionary];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        self.parser = [[NSXMLParser alloc] initWithData:data];
        self.parser.delegate = self;
        [self.parser parse];
    });
}

- (NSDate *)dateFromDict:(NSDictionary *)dict
{
    NSString *day = dict[@"day"];
    if (!day) {
        NSLog(@"Failed parsing date dictionary, no day was given");
        return nil;
    }
    NSString *month = dict[@"month"];
    if (!month) {
        NSLog(@"Failed parsing date dictionary, no month was given");
        return nil;
    }
    NSString *year = dict[@"year"];
    if (!year) {
        NSLog(@"Failed parsing date dictionary, no year was given");
        return nil;
    }
    NSString *hour = dict[@"hour"];
    if (!hour) {
        NSLog(@"Failed parsing date dictionary, no hour was given");
        return nil;
    }
    NSString *dateString = [NSString stringWithFormat:@"%@-%@-%@ %@", year, month, day, hour];
    return [[self dateFormatter] dateFromString:dateString];
}

#pragma mark - XMLParser delegate

static NSString *const townKey = @"TOWN";
static NSString *const forecastKey = @"FORECAST";

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:townKey]) {
        if (!self.resultDict[townKey]) {
            self.resultDict[townKey] = [NSMutableArray array];
        }
        self.currentTown = [attributeDict mutableCopy];
    } else if ([elementName isEqualToString:forecastKey]) {
        if (!self.currentTown[forecastKey]) {
            self.currentTown[forecastKey] = [NSMutableArray array];
        }
        self.currentForecast = [attributeDict mutableCopy];
        self.currentForecast[@"date"] = [self dateFromDict:attributeDict];
    } else {
        if (self.currentForecast) {
            self.currentForecast[elementName] = attributeDict;
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:townKey]) {
        if (!self.resultDict[townKey]) {
            self.resultDict[townKey] = [NSMutableArray array];
        }
        [self.resultDict[townKey] addObject:self.currentTown];
        self.currentTown = nil;
    } else if ([elementName isEqualToString:forecastKey]) {
        if (!self.currentTown[forecastKey]) {
            self.currentTown[forecastKey] = [NSMutableArray array];
        }
        [self.currentTown[forecastKey] addObject:self.currentForecast];
        self.currentForecast = nil;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if (self.parseCompletion) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.parseCompletion(self.resultDict, nil);
        });
        
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    if ([parseError code] != NSXMLParserDelegateAbortedParseError) {
        if (self.parseCompletion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.parseCompletion(nil, parseError);
            });
        }
    }
}
@end
