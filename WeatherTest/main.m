//
//  main.m
//  WeatherTest
//
//  Created by Антон Тютин on 20.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
