//
//  TimeOfDayCollectionViewCell.h
//  WeatherTest
//
//  Created by Антон Тютин on 21.01.15.
//  Copyright (c) 2015 tapok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForecastCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *timeOfDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;

@end
